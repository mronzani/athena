#
# Package filtering rules for the Athena project build.
#

# Technical package(s) that should not show up in packages.txt:
- Build

# Ignore the Projects directory:
- Projects/.*

# Remove most HLT packages
# We currently build the HLT as part of Athena. Uncomment the following three lines to revert:
#+ HLT/Trigger/TrigTransforms/TrigTransform
#- HLT/.*
#- Trigger/TrigValidation/TrigP1Test

# Offload service has build problems
- Offloading/.*

# Abandoned generator package(s):
- Generators/Herwigpp_i

# Temporary VP1 compilation issues
- graphics/VP1/VP1Systems/VP1TriggerSystems
- graphics/VP1/VP1Systems/VP12DGeometrySystems
- graphics/VP1/VP1Systems/VP1MCSystems
- graphics/VP1/VP1Systems/VP1TriggerDecisionSystems
+ graphics/VP1/.*

# Some analysis packages that are not part of Athena
- Control/AthLinksSA
- PhysicsAnalysis/AnalysisCommon/CPAnalysisExamples
- PhysicsAnalysis/AnalysisCommon/PMGTools
- PhysicsAnalysis/D3PDTools/EventLoop.*
- PhysicsAnalysis/D3PDTools/MultiDraw
- PhysicsAnalysis/D3PDTools/SampleHandler
- PhysicsAnalysis/ElectronPhotonID/PhotonEfficiencyCorrection
- PhysicsAnalysis/ElectronPhotonID/PhotonVertexSelection
- PhysicsAnalysis/HiggsPhys/Run2/HZZ/Tools/ZMassConstraint
- PhysicsAnalysis/JetPhys/SemileptonicCorr
- PhysicsAnalysis/SUSYPhys/SUSYTools
- PhysicsAnalysis/TauID/DiTauMassTools
#- PhysicsAnalysis/TauID/TauCorrUncert  #no CMakeLists.txt file
- PhysicsAnalysis/TopPhys/QuickAna
- PhysicsAnalysis/TopPhys/TopPhysUtils/.*
- PhysicsAnalysis/TopPhys/xAOD/.*
- PhysicsAnalysis/TrackingID/InDetTrackSystematicsTools
- Reconstruction/Jet/JetAnalysisTools/JetTileCorrection
- Reconstruction/Jet/JetJvtEfficiency
- Reconstruction/Jet/JetReclustering
- Trigger/TrigAnalysis/TrigTauAnalysis/TrigTauMatching
- Trigger/TrigFTK/FTKStandaloneMonitoring

# Data quality packages not needed in Athena
- DataQuality/DataQualityConfigurations
- DataQuality/DCSCalculator2

# obsolete packages to be removed in a second step
- Tracking/TrkTools/TrkSegmentConverter
- Trigger/TrigAlgorithms/TrigEFLongLivedParticles

# Huge D3PD librarys not really useful any more
- PhysicsAnalysis/D3PDMaker/InDetD3PDMaker
- PhysicsAnalysis/D3PDMaker/CaloSysD3PDMaker

# Don't build PerfMonVTune which has external Intel tool dependency
- Control/PerformanceMonitoring/PerfMonVTune

# Old packages that don't work with AthenaMT
- LArCalorimeter/LArSim
- PhysicsAnalysis/HiggsPhys/HSG5/HSG5DPDUtils
